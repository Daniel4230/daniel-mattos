package main

import ( "net/http"	)

func main() {

	// declaração e atribuição do servidor http
	server := http.Server{Addr: "172.22.51.149:8083"}

	// servindo cebolas roxas
	http.HandleFunc("/request-info", req_info)

	// inicializar o servidor
	server.ListenAndServe()
}

func req_info(resp http.ResponseWriter, req *http.Request) {

	m := req.Method
	host := req.Host
	path := req.URL.String()
	_ = req.Header

	resp.Write([]byte("method: "  + m + "   path: " + host + path + "   header: "))
}